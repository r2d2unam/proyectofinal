/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/app-route/app-location.js';
import '@polymer/iron-image/iron-image.js';
import './shared-styles.js';


class MyView1 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }

        paper-button.pink {
          background-color: var(--paper-pink-400);
          color: white;
        }

        paper-button.pink-custom {
          background-color: var(--paper-pink-200);
          color: white;
        }
 

      </style>

      <iron-ajax id="api" 
                     url="http://localhost:3000/usuarios" 
                     method="GET" 
                     handle-as="json"
                     on-response="handleResponse"></iron-ajax>
        

      <div class="card">
        <div class="circle"><iron-icon icon="icons:account-circle"></iron-icon></div>
        <h1>[[prop1]]</h1>
      
        <paper-input label="user" id="user"></paper-input>
        <paper-input label="password" type="password" id="password"></paper-input>
        <paper-button raised class="pink" id="button" on-click="valida">Iniciar Sesion</paper-button>
        <paper-button raised class="pink-custom" id="button" on-click="registrar">Registrar</paper-button>
        <app-location route="{{route}}"></app-location>
        
          <h2>Login [[login]]</h2>

        <ul>
              <template is="dom-repeat" items="[[elements]]">
                  <li>[[usuarios.user]]</li>
                  <li>[[usuarios.password]]</li>
              </template>
        </ul>
      </div>

      

    `;
  }

  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'Bienvenido',
      }, element: {
            type: Array,
            value: []
        },usuarios: {
            type: Array,
            value: []
        },
        login:{
          type: Boolean,
            value: false
        }
    };
  }

  registrar() {
    console.log("registar clicked");
    this.set('route.path', '/view4');
  }

  valida() {
    console.log("clicked");
    const usuario = this.$.user.value;
    const contra = this.$.password.value;

    const data = {usuario, contra};
    this.$.api.body = JSON.stringify(data);
    console.log('data:', data);
    this.set('element', data);
    this.$.api.generateRequest();
    console.log("after Request");
}

compare( userMon, passMon ){
  const userform = this.element.usuario.toString();
  const passform = this.element.contra.toString();
  if(userMon == userform && passform == passMon ){
      console.log("login succes");
      this.set('login', true);
  }else{
      console.log("login fail");
  }
}

handleResponse() {
    const response = this.$.api.lastResponse;
    console.log('response:', response);
    console.log("before usuarios");
    let mongoresp = response;
    for (let mongo of mongoresp){
        console.log(mongo.user);
        console.log(mongo.password);
        this.compare(mongo.user,mongo.password);
    }

    console.log(this.login);

    if(this.login){
      this.set('route.path', '/view3');
    }else{
      console.log("usuario no login");
    }

  }

}

window.customElements.define('my-view1', MyView1);

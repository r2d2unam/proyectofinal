/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-list/iron-list.js';
import '@polymer/iron-ajax/iron-ajax.js';
import './shared-styles.js';

class MyView2 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }

        paper-button.pink {
          background-color: var(--paper-pink-400);
          color: white;
        }

      </style>

      <iron-ajax id="api" 
                     url="http://localhost:3000/movimientos" 
                     method="GET" 
                     handle-as="json"
                     on-response="handleResponse"></iron-ajax>
         
      <paper-button raised class="pink" on-click="consultamovimientos">Consultar Movimientos</paper-button>  
                            
      <template is="dom-repeat" items="[[movimientos]]">
        <div class="card">
          <div class="circle">3</div>        
          <h2>Movimiento</h2>
            <li>Cliente: [[item.idcliente]]</li>
            <li>Nombre:  [[item.nombre]]</li>
            <li>Apellidos: [[item.apellido]]</li>            
            <li>Compra: [[item.compra]]</li>
            <li>Retiro: [[item.retiro]]</li>
            <li>Abono: [[item.abono]]</li>
            <li>Devoluciones: [[item.devoluciones]]</li>            
            <li>Fecha: [[item.fecha]]</li>
            <li>Importe: [[item.importe]]</li>
            <li>Categoria: [[item.categoria]]</li>            
        </div>                                                     
      </template>      
      
      <!-- <paper-dropdown-menu label="Selecciona movimiento: ">
      <paper-listbox slot="dropdown-content">
        <paper-item>compra 1</paper-item>
        <paper-item>compra 2</paper-item>
        <paper-item>compra 3</paper-item>
        <paper-item>compra 4</paper-item>
      </paper-listbox>
      </paper-dropdown-menu>
      <paper-icon-button icon="find-in-page" title="find"></paper-icon-button>
      <iron-list items="[[items]]">
        <template>
          <div>

          </div>
        </template>
      </iron-list>
      </h3>
      <h4>Dar de alta movimiento:</h4>
      <label>Tipo de movimiento: </label>
      <paper-dropdown-menu label="Selecciona tipo movimiento: ">
      <paper-listbox slot="dropdown-content">
      <paper-item>compras</paper-item>
      <paper-item>retiro</paper-item>
      <paper-item>abono</paper-item>
      <paper-item>devolucion</paper-item>
      </paper-listbox>
      </paper-dropdown-menu>
      <paper-button raised class="indigo">Alta</paper-button> -->
         
    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'Bienvenido',
      }, movimientos: {
            type: Array,
            value: []
        },moves: {
          type: Array,
          value: []
        },usuarios: {
            type: Array,
            value: []
        },
        login:{
          type: Boolean,
            value: false
        }
    };
  }

  consultamovimientos() {
    console.log("clicked");     
    this.$.api.body = JSON.stringify();       
    this.$.api.generateRequest();
    console.log("after Request");
}

handleResponse() {
    const response = this.$.api.lastResponse;
    this.set('movimientos',response);
    console.log('response:', response);          
}

}

window.customElements.define('my-view2', MyView2);

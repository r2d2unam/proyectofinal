/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-list/iron-list.js';
import '@polymer/iron-ajax/iron-ajax.js';
import './my-view2';
import './shared-styles.js';


class MyView3 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }

        paper-button.pink {
          background-color: var(--paper-pink-400);
          color: white;
        }
        
      </style>      

      <iron-ajax id="api" 
                     url="http://localhost:3000/cuentas" 
                     method="GET" 
                     handle-as="json"
                     on-response="handleResponse"></iron-ajax>

      <div class="card">
      <h2>[[prop1]]</h2>
      <paper-button raised class="pink" on-click="consultacuentas">Consultar Cuentas</paper-button>                        
      <my-view2></my-view2>                   
      </div>
      
      <template is="dom-repeat" items="[[cuentas]]">
        <div class="card">
          <div class="circle">3</div>        
          <h2>Cuenta</h2>
                  <li>Cuenta: [[item.name]]</li>
                  <li>Numero: [[item.number]]</li>
                  <li>Descripcion: [[item.description]]</li>
                  <li>Estatus: [[item.status]]</li>
                  <li>Tipo: [[item.type]]</li>
        </div>                                                     
      </template>      

      <!-- <paper-dropdown-menu label="Selecciona tu cuenta: ">
      <paper-listbox slot="dropdown-content">
        <paper-item>cuenta 1</paper-item>
        <paper-item>cuenta 2</paper-item>
        <paper-item>cuenta 3</paper-item>
        <paper-item>cuenta 4</paper-item>
      </paper-listbox>
    </paper-dropdown-menu>
    <paper-icon-button icon="find-in-page" title="find"></paper-icon-button>
    <iron-list items="[[items]]">
        <template>
          <div>

          </div>
        </template>
      </iron-list>
    </h3>
    <h4>Dar de alta cuenta:</h4>
    <label>Tipo de cuenta: </label>
    <paper-dropdown-menu label="Selecciona tu cuenta: ">
    <paper-listbox slot="dropdown-content">
      <paper-item>cheques</paper-item>
      <paper-item>préstamo</paper-item>
      <paper-item>TDC</paper-item>
      <paper-item>hipotecario</paper-item>
    </paper-listbox>
  </paper-dropdown-menu>
  <paper-button raised class="indigo">Alta</paper-button> -->

      
    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'Bienvenido',
      }, cuentas: {
        type: Array,
        value: []
      },
        usuarios: {
            type: Array,
            value: []
        },
        login:{
          type: Boolean,
            value: false
        }
    };
  }

  consultacuentas() {
    console.log("clicked");     
    this.$.api.body = JSON.stringify();       
    this.$.api.generateRequest();
    console.log("after Request");
}

handleResponse() {
    const response = this.$.api.lastResponse;
    this.set('cuentas',response);
    console.log('response:', response);          
}

}

window.customElements.define('my-view3', MyView3);

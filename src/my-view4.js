/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';

import './shared-styles.js';


class MyView4 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }

        paper-button.pink {
          background-color: var(--paper-pink-400);
          color: white;
        }
 

      </style>

      <iron-ajax id="api" 
                    url="http://localhost:3000/usuarios" 
                    method="POST"                   
                    handle-as="json"
                    content-type="application/json"                    
                    on-response="handleResponse"></iron-ajax>

      <div class="card">
        <div class="circle"><iron-icon icon="icons:account-circle"></div>        
      
      <h2>[[prop1]] Registro  [[login]]</h2>      
      <paper-input id="email" label="your-name@example.com"></paper-input>            
      <paper-input id="nombres" label="nombres"></paper-input>      
      <paper-input id="apellidos" label="apellidos"></paper-input>            
      <paper-input id="user" label="user"></paper-input>      
      <paper-input id="password" type="password" label="******" ></paper-input>      

      <paper-button id="button" raised class="pink" on-click="clicked">Registrar</paper-button>                    

        <ul>
              <template is="dom-repeat" items="[[elements]]">
                  <li>[[usuarios.user]]</li>
                  <li>[[usuarios.password]]</li>
              </template>
        </ul>
      </div>

    `;
  }

  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'Bienvenido',
      }, element: {
            type: Array,
            value: []
        },
        registro:{
          type: Boolean,
            value: false
        }
    };
  }

  clicked() {
    console.log("clicked");
    const email = this.$.email.value;    
    const nombres = this.$.nombres.value;
    const apellidos = this.$.apellidos.value;
    const user = this.$.user.value;    
    const password = this.$.password.value;   

    const data = {email, nombres, apellidos, user, password};
    this.$.api.body = JSON.stringify(data);
    console.log('data:', data);
    this.set('element', data);
    this.$.api.generateRequest();
    console.log("after Request");
}

handleResponse() {
    const response = this.$.api.lastResponse;
    console.log('response:', response);   
  }

}

window.customElements.define('my-view4', MyView4);
